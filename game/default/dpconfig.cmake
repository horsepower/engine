option(
ENGINE_BUILD_CLIENT
"Build the client"
ON)

option(
ENGINE_BUILD_SERVER
"Build the server"
ON)

option_string(
ENGINE_BUILD_NAME
"Engine binary name"
"darkplaces")

option_string(
ENGINE_BUILD_WINRC
"Location of Windows .rc, usually for icon"
"${GAME_PROJECT_DIR}/res/darkplaces.rc")

option_string(
GAME_BUILD_CUSTOM_COMMAND
"Custom tool to bootstrap. Enter what you would with execute_process"
"")

option_string(
GAME_BUILD_EXTERNAL_PROJECT
"External cmake project to add. Enter what you would with ExternalProject_Add"
"")

option(
ENGINE_CONFIG_MENU
"Enable menu support"
ON)

option(
ENGINE_CONFIG_VIDEO_CAPTURE
"Enable video capture support"
ON)

option(
ENGINE_CONFIG_BUILD_TIMESTAMP
"Add the git commit timestamp to the version string"
ON)

option(
ENGINE_CONFIG_BUILD_REVISION
"Add the git revision to the version string"
ON)

option(
ENGINE_CONFIG_PRVM_GC
"(EXPERIMENTAL) Enable PRVM garbage collection of tempstrings.
Currently causes a memory leak and console spam in Xonotic"
)

option(
ENGINE_LINK_TO_CRYPTO
"Link to d0_blind_id dynamically"
ON)

option(
ENGINE_LINK_TO_CRYPTO_RIJNDAEL
"Link to d0_blind_id_rijndael dynamically"
ON)

option(
ENGINE_LINK_TO_ZLIB
"Link to zlib dynamically"
ON)

option(
ENGINE_LINK_TO_LIBJPEG
"Link to libjpeg dynamically"
ON)

option(
ENGINE_LINK_TO_LIBODE
"Link to ODE dynamically"
OFF)