macro(hp_set_flags)
	if(UNIX)
		set(ENGINE_CFLAGS "\
			-Wall \
			-Wold-style-definition \
			-Wstrict-prototypes \
			-Wdeclaration-after-statement \
			-Wmissing-prototypes \
			-Wsign-compare \
			-Wno-pointer-sign \
			-Wno-unknown-pragmas \
			-Wno-format-zero-length \
			-Wno-strict-aliasing \
			-fno-math-errno \
			-fno-rounding-math \
			-fno-signaling-nans \
			-fno-trapping-math \
			-Dstrnicmp=strncasecmp \
			-Dstricmp=strcasecmp"
		)
		set(CMAKE_C_FLAGS_RELEASE "-O3 -fno-strict-aliasing")
	elseif(MSVC)
		#TODO
		set(ENGINE_CFLAGS
			"/Wall"
		)
	endif()

	set(ENGINE_DFLAGS "${ENGINE_DFLAGS} -MMD")

	if(ENGINE_VERSION)
		set(ENGINE_DFLAGS "${ENGINE_DFLAGS} -DSVNREVISION='${ENGINE_VERSION}'")
	endif()

	if(CMAKE_BUILD_TYPE)
		set(ENGINE_DFLAGS "${ENGINE_DFLAGS} -DBUILDTYPE='${CMAKE_BUILD_TYPE}'")
	endif()

	if(ENGINE_NO_BUILD_TIMESTAMPS)
		set(ENGINE_DFLAGS "${ENGINE_DFLAGS} -DNO_BUILD_TIMESTAMPS")
	endif()

	if(ENGINE_CONFIG_PRVM_GC)
		set(ENGINE_DFLAGS "${ENGINE_DFLAGS} -DCONFIG_PRVM_GC")
	endif()

	if(UNIX)
		set(ENGINE_LIBS "-lm") # Every Unix platform will include this.
		if(WIN_GNU) # MinGW
			set(ENGINE_LIBS "${ENGINE_LIBS} -lwinmm -limm32 -lversion -lwsock32 -lws2_32")
		else() # Literally anything else
			set(ENGINE_LIBS "${ENGINE_LIBS} -ldl -lz")
			if(NOT APPLE)
				set(ENGINE_LIBS "${ENGINE_LIBS} -lrt")
				if(SUN_OS)
					set(ENGINE_LIBS "${ENGINE_LIBS} -lsocket -lnsl")
				endif()
			else()
				set(ENGINE_LIBS "${ENGINE_LIBS} -framework IOKit -framework CoreFoundation")
			endif()
		endif()
	endif()

	set(ENGINE_FLAGS "${ENGINE_CFLAGS} ${ENGINE_DFLAGS}")
endmacro()