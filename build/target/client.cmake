#------------------------------------------------------------------------------#
#   Copyright (c) 2020 Cloudwalk                                               #
#                                                                              #
#   This program is free software: you can redistribute it and/or modify       #
#   it under the terms of the GNU General Public License as published by       #
#   the Free Software Foundation, either version 3 of the License, or          #
#   (at your option) any later version.                                        #
#                                                                              #
#   This program is distributed in the hope that it will be useful,            #
#   but WITHOUT ANY WARRANTY; without even the implied warranty of             #
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              #
#   GNU General Public License for more details.                               #
#                                                                              #
#   You should have received a copy of the GNU General Public License          #
#   along with this program.  If not, see <http://www.gnu.org/licenses/>.      #
#------------------------------------------------------------------------------#

add_executable(client)

find_package(SDL2 REQUIRED)
find_package(PNG REQUIRED)
find_package(CURL REQUIRED)
find_package(ZLIB REQUIRED)

set(ENGINE_CL_INCLUDES ${SDL2_INCLUDE_DIR} ${PNG_INCLUDE_DIR} ${CURL_INCLUDE_DIR} ${ZLIB_INCLUDE_DIRS})
set(ENGINE_CL_LIBS "${SDL2_LIBRARY} ${PNG_LIBRARY} ${CURL_LIBRARY} ${ZLIB_LIBRARIES}")

if(ENGINE_CONFIG_MENU)
	set(ENGINE_CL_DFLAGS "${ENGINE_CL_DFLAGS} -DCONFIG_MENU")
endif()

if(ENGINE_LINK_TO_CRYPTO OR ENGINE_LINK_TO_CRYPTO_RIJNDAEL)
	find_package(Crypto REQUIRED)
	set(ENGINE_CL_INCLUDES ${ENGINE_CL_INCLUDES} ${CRYPTO_INCLUDE_DIR})
	if(ENGINE_LINK_TO_CRYPTO)
		set(ENGINE_CL_DFLAGS "${ENGINE_CL_DFLAGS} -DLINK_TO_CRYPTO")
		set(ENGINE_CL_LIBS "${ENGINE_CL_LIBS} ${d0_blind_id_LIBRARY}")
	endif()
	if(ENGINE_LINK_TO_CRYPTO_RIJNDAEL)
		set(ENGINE_CL_DFLAGS "${ENGINE_CL_DFLAGS} -DLINK_TO_CRYPTO_RIJNDAEL")
		set(ENGINE_CL_LIBS "${ENGINE_CL_LIBS} ${d0_rijndael_LIBRARY}")
	endif()
endif()

if(ENGINE_LINK_TO_LIBJPEG)
	find_package(JPEG REQUIRED)
	set(ENGINE_CL_INCLUDES ${ENGINE_CL_INCLUDES} ${JPEG_INCLUDE_DIR})
	set(ENGINE_CL_DFLAGS "${ENGINE_CL_DFLAGS} -DLINK_TO_LIBJPEG")
	set(ENGINE_CL_LIBS "${ENGINE_CL_LIBS} ${JPEG_LIBRARY}")
endif()

if(ENGINE_LINK_TO_LIBVORBIS)
	find_package(Vorbis REQUIRED)
	set(ENGINE_CL_INCLUDES ${ENGINE_CL_INCLUDES} ${VORBIS_INCLUDE_DIRS})
	set(ENGINE_CL_DFLAGS "${ENGINE_CL_DFLAGS} -DLINK_TO_LIBVORBIS")
	set(ENGINE_CL_LIBS "${ENGINE_CL_LIBS} ${VORBIS_LIBRARIES}")
endif()

if(ENGINE_LINK_TO_LIBODE)
	set(ENGINE_CL_DFLAGS "${ENGINE_CL_DFLAGS} -DUSEODE")
endif()

if(ENGINE_CONFIG_VIDEO_CAPTURE)
	set(ENGINE_CL_DFLAGS "${ENGINE_CL_DFLAGS} -DCONFIG_VIDEO_CAPTURE")
endif()

set(ENGINE_CL_FLAGS "${ENGINE_FLAGS} ${ENGINE_CL_DFLAGS}")
set(ENGINE_CL_LIBS "${ENGINE_CL_LIBS} ${ENGINE_LIBS}")
set(ENGINE_CL_INCLUDES ${INC_DIR} ${ENGINE_CL_INCLUDES})

target_sources(client PRIVATE
	"${OBJ_CL}"
	"${OBJ_MENU}"
	"${OBJ_SND_COMMON}"
	"${OBJ_CD_COMMON}"
	"${OBJ_VIDEO_CAPTURE}"
	"${OBJ_COMMON}"
)

if(WIN32)
	target_sources(client PRIVATE ${ENGINE_BUILD_WINRC})
endif()

#add_dependencies(client d0_blind_id SDL2 zlib PNG JPEG curl)

set_target_properties(client PROPERTIES
	OUTPUT_NAME "${ENGINE_BUILD_NAME}"
	COMPILE_FLAGS "${ENGINE_CL_FLAGS}"
)

target_link_libraries(client "${ENGINE_CL_LIBS}")

target_include_directories(client PRIVATE ${ENGINE_CL_INCLUDES})
