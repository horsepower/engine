# Build a version string for the engine.
function(hp_build_get_version)
	set(ENV{TZ} "UTC")

	execute_process(
		COMMAND git rev-parse --short HEAD
		WORKING_DIRECTORY ${CMAKE_SOURCE_DIR}
		OUTPUT_VARIABLE revision
		OUTPUT_STRIP_TRAILING_WHITESPACE
	)

	execute_process(
		COMMAND "git show -s --format=%ad --date='format-local:%a %b %d %Y %H:%I:%S UTC'"
		OUTPUT_VARIABLE timestamp
		OUTPUT_STRIP_TRAILING_WHITESPACE
	)
	set(HP_REVISION "${timestamp} - ${revision}")
endfunction()

function(hp_build project path)
	if(NOT path)
		if(project)
			set(path ${HP_DIR}/game/${project})
		endif()
	endif()

	if(path)
		include(${path}/dpconfig.cmake)
	endif()

	if(ENGINE_EXE_NAME STREQUAL "") # Cannot be empty
        message(FATAL_ERROR "You must give the executable a name.")
    endif()

    if(ENGINE_EXE_NAME MATCHES "[* *]") # Cannot contain spaces.
        message(FATAL_ERROR "The executable name must not contain spaces.")
	endif()

	if(NOT ENGINE_BUILD_CLIENT AND NOT ENGINE_BUILD_SERVER)
		message(FATAL_ERROR "You must build at least one target.")
	endif()

	hp_set_flags()

	hp_build_get_version()

	if(ENGINE_BUILD_CLIENT)
		include(build/target/client.cmake)
	endif()

	if(ENGINE_BUILD_SERVER)
		include(build/target/server.cmake)
	endif()

	if(GAME_BUILD_EXTERNAL_PROJECT)
		include(ExternalProject)
		ExternalProject_Add(${GAME_BUILD_EXTERNAL_PROJECT})
	endif()
endfunction()