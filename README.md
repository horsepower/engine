# Horsepower/Darkplaces Engine

## Setup
Below are platform-specific steps to grab dependencies and set up the build environment. See below for the actual compile instructions.

### Windows (MSYS2):

1. Install MSYS2, found [here](https://www.msys2.org/).
2. Once you've installed MSYS2 and have fully updated it, open an MSYS2 terminal and input the following command:

```
pacman -S --needed gcc make cmake mingw-w64-x86_64-{toolchain,libjpeg-turbo,libpng,libogg,libvorbis,sdl2}
```

### Windows (Visual Studio):

While possible, the build script requires Bash anyway, which can be found in
Git. No instructions are available yet and you'll have to grab the dependencies
yourself. Assuming you've done all of this, skip to "Compile".

### Debian/Ubuntu:

1. Install the packages by entering the following command in a terminal as root:

```
apt install build-essential cmake lib{sdl2,jpeg,png,vorbis,ogg}-dev
```

### MacOS:

1. Install Xcode and CMake.
2. Open a terminal and enter this command (as root) to install Xcode's and
CMake's command-line tools:

```
xcode-select --install && sudo "/Applications/CMake.app/Contents/bin/cmake-gui" --install
```
3. Install Homebrew by entering the following command.

```
/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
```
4. Install the packages by entering the following command as root:

```
brew install sdl2 libjpeg libpng libvorbis
```

### FreeBSD:

1. Install the packages by entering the following command in a terminal as root:
```
pkg install cmake SDL2 jpeg png vorbis
```

### OpenIndiana:

1. Install the packages by entering the following command in a terminal as root:

```
pkg install build-essential gcc-8 SDL2 libjpeg libpng libvorbis
```

### Android:

Coming soon.

## Compile

### Using build.sh
1. Change directory to the root of the repository if you haven't already.
2. Run ./build.sh and follow the instructions.

See ./build.sh --help for documentation of the script.

### Using CMake directly
Run the following command:
```
cmake -B<location to copy build files> .
```

Optionally, you may use a GUI or TUI frontend such as cmake-gui or ccmake respectively, or your IDE of choice using CMake's server mode.

If you wish to change the compiler, you may prepend the CC and CXX environment variables to the command-line, or add them to the build configuration of your IDE.

**NOTE**: *In-source builds will not work properly with the Eclipse CDT4 generator. Make sure you specify a directory outside of the repository root if you use it.*

### Engine Configuration

The build system takes a dpconfig.cmake file which specifies configuration options for the engine (to enable or disable specific features your game may or may not need).

The default engine configuration is located in game/default/dpconfig.cmake. It is loaded no matter what.

You may supply your own dpconfig.cmake file (which will override the default), or more specifically, a directory containing one, in two ways:

1. If you wish to use an existing project within the 'game' subdirectory, you may append "GAME_PROJECT=\<name\>" to your CMake command-line.

2. To point CMake to a project outside of the source directory, you may also use "GAME_PROJECT_DIR=\<directory\>"

**NOTE**: *The directory specified in GAME_PROJECT_DIR must contain a dpconfig.cmake or the configuration will fail. Furthermore, it will be defined automatically if left blank and GAME_PROJECT is set.*

Below is an example of a full command-line string instructing CMake to use a project called "testing" with the build directory set to "output/testing", and using clang and clang++.

```
CC="clang" CXX="clang++" cmake -B./output/testing . -DGAME_PROJECT="testing"
```